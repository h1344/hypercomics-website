---
id: 111
title: The other stuff2
originaltitle: 「諸国滝廻り　木曾路ノ奥阿弥陀ヶ瀧」
author: Katsushika Hokusai (Japanese, 1760–1849)
publisher: Nishimuraya Yohachi (Eijudô) (Japanese)
origin: Japanese
circa: 'Edo period
about 1832 (Tenpô 3)'
medium: Woodblock print (nishiki-e); ink and color on paper
dimensions: Vertical ôban; 36.1 x 25.7 cm (14 3/16 x 10 1/8 in.)
credit: William S. and John T. Spaulding Collection
acquisitionnumber: 21.6687
collections: Asia, Prints and Drawings
classifications: Prints
filename: test.jpg
---