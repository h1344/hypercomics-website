---
class: homepage
title: hypercomics
layout: homepage.njk
permalink: /index.html
backroundnoticeid: 187269
backgroundclass: "halftone"
---

What if we could make a comic book that would use all the benefits of the web to make open access sequential narrative a thing?
