

module.exports = function (eleventyConfig) {


  eleventyConfig.addPassthroughCopy({ "static/css": "/css" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "/fonts" });
  eleventyConfig.addPassthroughCopy({ "static/js": "/js" });
  eleventyConfig.addPassthroughCopy({ "static/images": "/images" });
  eleventyConfig.addPassthroughCopy({ "static/videos": "/videos" });


  let filters = `{% import "../../layouts/macros.njk" as macro with context %}`

  eleventyConfig.addCollection("notices", collection => {
    return [...collection.getFilteredByGlob("src/content/notices/**/*.md")]
  });
  eleventyConfig.addCollection("posts", collection => {
    collection = collection.getFilteredByGlob("src/content/posts/**/*.md");
    collection.forEach(el => {
      // add macros on the fly to the collection
      el.template.inputContent = el.template.inputContent.replace('---\n\n', `---\n\n${filters}\n`)
      el.template.frontMatter.content = `${filters}\n${el.template.frontMatter.content}`
      console.log(el.template.inputContent)
    })
    return collection;
  });



  // folder structures
  // -----------------------------------------------------------------------------
  // content, data and layouts comes from the src folders
  // output goes to public (for gitlab ci/cd)
  // -----------------------------------------------------------------------------
  return {
    // run the md through the njk engine first to use macro
    markdownTemplateEngine: "njk",
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
